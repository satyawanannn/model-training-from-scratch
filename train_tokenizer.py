import argparse
import os
from transformers import BartTokenizer
from pathlib import Path
from tokenizers import ByteLevelBPETokenizer

parser = argparse.ArgumentParser(description="Train Tokenizer")
parser.add_argument("-dt","--datafiledir",required=True, type=str,help="Input your data file directory here")
parser.add_argument("-td","--tokenizerdir",required=True,type=str,help="Input the folder where your tokenizer will be saved")
# parser.add_argument("-vj","--vocabjson",required=True,type=str,help="Input your vocab.json file directory here")
# parser.add_argument("-mt","--mergestxt",required=True,type=str,help="Input your merges.txt file directory here")

args = parser.parse_args()

tokenizer = ByteLevelBPETokenizer()
tokenizer.train(files=args.datafiledir, min_frequency=2, special_tokens=[
    "<s>",
    "<pad>",
    "</s>",
    "<unk>",
    "<mask>"
])

tokenizer.save_model(args.tokenizerdir)

tokenizer = BartTokenizer(
  args.tokenizerdir+"vocab.json",args.tokenizerdir+"merges.txt",
  do_lower_case=True, 
  do_basic_tokenize=True,
  padding=True,bos_token='[CLS]', 
  eos_token='[SEP]', sep_token='[SEP]', 
  cls_token='[CLS]', unk_token='[UNK]', 
  pad_token='[PAD]', mask_token='[MASK]',)

tokenizer.save_pretrained(args.tokenizerdir)