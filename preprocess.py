import os
import re
import spacy
import argparse

def get_args():
    parser = argparse.ArgumentParser(description="Preprocess Data. all param required if no method selected")
    parser.add_argument('-d','--datadir', type=str, required=True, help="Input your dataset folder path")
    parser.add_argument('-dst','--destdir', type=str, help="Input your new dataset folder dir")
    parser.add_argument('-md','--merged_datadir',type=str,help="Input folder for merged dataset")
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-rl', '--removelines', action='store_true', help="remove unnecessary new lines. required param : -d")
    group.add_argument('-tl', '--tolines', action='store_true', help="split sentences into lines. required param : -d, -dst")
    group.add_argument('-l', '--lower', action='store_true', help="lowercase all character in file. required param : -d")
    group.add_argument('-mf', '--mergefiles', action='store_true', help="merge all data files into single file. required param : -d, -md")
    
    return parser.parse_args()

def remove_newlines(filedir):
    files = os.listdir(filedir)
    
    for fn in files:
    	if(fn.endswith('.txt')):
    		f = open(filedir+fn,'r+',encoding='utf-8')
    		text = f.read()
    		text = re.sub('\n',' ',text)
    		f.seek(0)
    		f.write(text)
    		f.truncate()

def sent_to_line(filedir, destdir):
    nlp = spacy.load('en_core_web_sm')
    files = os.listdir(filedir)

    for f in files:
        if(f.endswith('.txt')):
            file = open(filedir+f, 'r', encoding='utf-8')
            text = file.read()
    
            linetext = nlp(text)
    
            file = open(destdir+f, 'w+', encoding='utf-8')
            for i in linetext.sents:
                i = i.string.strip()
                
                file.write(i+'\n')
            file.close()
            
def lowercase(datadir):
    files = os.listdir(datadir)

    for file in files:
        f = open(datadir+file,'r+',encoding='utf-8')
        text = f.read()
        text = text.lower()
        f.seek(0)
        f.write(text)
        f.truncate()
    
def merge_files(datadir, destdir):
    paths = os.listdir(datadir)

    texts = []
    
    for p in paths:
    	if(p.endswith('.txt')):
    		t = open(datadir+p, 'r', encoding='utf-8').read()
    
    		texts.append(t)
    
    dataset = open(destdir+"dataset.txt",'w+', encoding='utf-8')
    
    for t in texts:
    	dataset.write(t)
    
    dataset.close()

if __name__=="__main__":
    args = get_args()
    
    if args.removelines:
        remove_newlines(args.datadir)
    elif args.tolines:
        sent_to_line(args.datadir, args.destdir)
    elif args.lower:
        lowercase(args.datadir)
    elif args.mergefiles:
        merge_files(args.datadir, args.merged_datadir)
    else:
        remove_newlines(args.datadir)
        sent_to_line(args.datadir, args.destdir)
        lowercase(args.destdir)
        merge_files(args.destdir, args.merged_datadir)
