import argparse, csv
import pandas as pd
from sentence_transformers import SentenceTransformer, models, InputExample, losses, evaluation
from torch.utils.data import DataLoader

parser = argparse.ArgumentParser(description="Train Sentence Transformers")
parser.add_argument("-bmd","--basemodeldir",required=True, type=str,help="Input base model directory")
parser.add_argument("-dmd","--destmodeldir",required=True, type=str,help="Input destination model directory")
parser.add_argument("-tdf","--traindatafile",required=True, type=str,help="Input train data file  here")
parser.add_argument("-edf","--evdatafile",required=True, type=str,help="Input evaluation data file directory here")
parser.add_argument("-bs","--batchsize",default=16, type=int,help="Input batch size here")
parser.add_argument("-e","--epoch",default=10, type=int,help="Input number of epoch here")

args = parser.parse_args()


#CREATE MODEL
word_embedding_model = models.Transformer(args.basemodeldir,max_seq_length=512)
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())

model = SentenceTransformer(modules=[word_embedding_model, pooling_model])
#-------------------------------------------------------------

#TRAIN DATA
training_data = []

with open(args.traindatafile) as f:
    csv_reader = csv.reader(f)
    for row in csv_reader:
        # print(row)
        training_data.append(InputExample(texts=[row[0],row[1]],label=float(row[2])))

train_dataloader = DataLoader(training_data,shuffle=True, batch_size=args.batchsize)
#------------------------------------------------------------

#FIT MODEL
df = pd.read_csv(args.evdatafile, header=None)
sent1=df[0]
sent1 = sent1.values.tolist()

sent2 = df[1]
sent2 = sent2.values.tolist()

scores = df[2]
scores = scores.values.tolist()

train_loss = losses.CosineSimilarityLoss(model)

evaluator = evaluation.EmbeddingSimilarityEvaluator(sent1, sent2, scores)

model.fit(train_objectives=[(train_dataloader, train_loss)], epochs=args.epoch, warmup_steps=100, evaluator=evaluator, evaluation_steps=100, output_path=args.destmodeldir)#-----------------------------------------------------------------------