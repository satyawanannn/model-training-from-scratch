import argparse, os
from transformers import BartTokenizer, BertConfig, BertForMaskedLM, LineByLineTextDataset, DataCollatorForLanguageModeling, Trainer, TrainingArguments

parser = argparse.ArgumentParser(description="Train Base Model")
parser.add_argument('-td','--tokendir', type=str, required=True, help="Input tokenizer folder path")
parser.add_argument('-df','--datfile', type=str, required=True, help="Input data file path")
parser.add_argument('-od','--outdir', type=str, required=True, help="Input model output folder path")
parser.add_argument('-vs','--vocabsize', default=60000, type=int, help="Input vocab size") #SERING JADI PENYEBAB EROR.OUT OF INDEX--> GEDEIN, OUT OF MEMORY --> KECILIN
parser.add_argument('-me','--maxemb', default=512, type=int, help="Input maximum embedding size (512, 1024, 2048, ...)")
parser.add_argument('-hs','--hidsize', default=384, type=int, help="Input hidden layer size(desired embedding length)")
parser.add_argument('-ah','--atthead', default=12, type=int, help="Input attention heads number")
parser.add_argument('-hl','--hidlay', default=6, type=int, help="Input number of hidden layer")
parser.add_argument('-e','--epoch', default=1, type=int, help="Input epoch")
parser.add_argument('-b','--batch', default=64, type=int, help="Input batch") #SERING JADI PENYEBAB EROR

args = parser.parse_args()

tokenizer = BartTokenizer.from_pretrained(args.tokendir)

config = BertConfig(
    vocab_size=args.vocabsize,#BISA JADI PENYEBAB EROR PAS TRAINING. coba digedein/samain ke  kalo misalnya ada eror "cuda assert"/"index out of range"
    max_position_embeddings=args.maxemb,
    hidden_size=args.hidsize,
    num_attention_heads=args.atthead,
    num_hidden_layers=args.hidlay,
    # type_vocab_size=1,
)
model = BertForMaskedLM(config=config)

dataset = LineByLineTextDataset(
    tokenizer=tokenizer,
    file_path='/content/drive/MyDrive/Work/Keyword Extraction/dataset.txt',
    block_size=512
)

data_collator = DataCollatorForLanguageModeling(
    tokenizer=tokenizer, mlm=True, mlm_probability=0.15
)

training_args = TrainingArguments(
    output_dir=args.outdir,
    overwrite_output_dir=True,
    num_train_epochs=args.epoch,
    per_device_train_batch_size=args.batch,
    save_steps=10_000,
    save_total_limit=2,
    prediction_loss_only=True,
)

trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=data_collator,
    train_dataset=dataset
)

trainer.train()
trainer.save_model(args.outdir)