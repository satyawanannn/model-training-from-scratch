import argparse
from keybert import KeyBERT
from sentence_transformers import SentenceTransformer, models

parser = argparse.ArgumentParser(description="Extract Keyword")
parser.add_argument("-afd","--artfiledir",required=True, type=str,help="Input article file directory here")
parser.add_argument("-smd","--stmodeldir",required=True, type=str,help="Input sentence transformer model directory here")
parser.add_argument("-nl","--ngramlower",default=1, type=int,help="Input lower ngram bound here")
parser.add_argument("-nu","--ngramupper",default=2, type=int,help="Input upper ngram bound here")
parser.add_argument("-sw","--stopwords",default=None, type=str,help="Input stopwords language here (only 'english' tested)")
parser.add_argument("-tn","--topn",default=10, type=int,help="Input top n keyword to be shown here")
parser.add_argument("-umm","--usemmr",default=False, type=bool,help="Use maximal marginal relevance?")
parser.add_argument("-ums","--usemaxsum",default=False, type=bool,help="Use max sum?")
parser.add_argument("-nc","--nrcandidates",default=20, type=int,help="Input nr candidates here if you are using max sum")
parser.add_argument("-div","--diversity",default=0.7, type=float,help="Input diversity here if you are using maximal marginal relevance")

args = parser.parse_args()

with open(args.artfiledir,'r',encoding="utf-8") as f:
    doc = f.read()

word_embedding_model = models.Transformer(args.stmodeldir,max_seq_length=384)
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
model = SentenceTransformer(modules=[word_embedding_model, pooling_model])
# model='paraphrase-MiniLM-L6-v2'

kw_model = KeyBERT(model=model)
keywords = kw_model.extract_keywords(doc, keyphrase_ngram_range=(args.ngramlower, args.ngramupper), stop_words=args.stopwords, top_n=args.topn, use_mmr=args.usemmr, use_maxsum=args.usemaxsum, nr_candidates=args.nrcandidates, diversity=args.diversity)

print(keywords)